﻿using System;
using System.Collections.Generic;
using System.Text;
using TestBrainence.DAL.Contracts;
using TestBrainence.Persistence;

namespace TestBrainence.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly TestBrainenceDbContext _context;
        private ISentenceRepo _sentences;

        public UnitOfWork(TestBrainenceDbContext context)
        {
            _context = context;
        }
        public ISentenceRepo Sentences => _sentences ?? (_sentences = new SentenceRepo(_context));

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
    }
}
