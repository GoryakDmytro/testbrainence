﻿using System;
using System.Collections.Generic;
using System.Text;
using TestBrainence.DAL.Contracts;
using TestBrainence.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace TestBrainence.DAL
{
    public class SentenceRepo : GenericRepository<SentenceEntity>, ISentenceRepo
    {
        public SentenceRepo(DbContext context) : base(context)
        {
        }
    }
}
