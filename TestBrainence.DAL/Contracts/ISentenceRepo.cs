﻿using System;
using System.Collections.Generic;
using System.Text;
using TestBrainence.Domain.Entities;

namespace TestBrainence.DAL.Contracts
{
    public interface ISentenceRepo : IGenericRepository<SentenceEntity>
    {
    }
}
