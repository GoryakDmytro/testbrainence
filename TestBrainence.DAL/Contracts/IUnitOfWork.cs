﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestBrainence.DAL.Contracts
{
    public interface IUnitOfWork
    {
        ISentenceRepo Sentences { get; }

        int SaveChanges();
    }
}
