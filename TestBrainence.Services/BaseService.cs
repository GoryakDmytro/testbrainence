﻿using System;
using System.Collections.Generic;
using System.Text;
using TestBrainence.DAL.Contracts;

namespace TestBrainence.Services
{
    public class BaseService
    {
        protected readonly IUnitOfWork _uow;

        public BaseService(IUnitOfWork unitOfWork)
        {
            _uow = unitOfWork;
        }
    }
}
