﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestBrainence.Models.Models;

namespace TestBrainence.Services.Contracts
{
    public interface ISentenceService
    {
        Task<IEnumerable<Sentence>> GetAllAsync();
        Task<Sentence> FindAsync(int sentenceId);
        Task<Sentence> AddAsync(Sentence sentence);
    }
}
