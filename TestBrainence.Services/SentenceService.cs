﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBrainence.DAL.Contracts;
using TestBrainence.Domain.Entities;
using TestBrainence.Models.Models;
using TestBrainence.Services.Contracts;

namespace TestBrainence.Services
{
    public class SentenceService : BaseService, ISentenceService
    {
        public SentenceService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        public async Task<IEnumerable<Sentence>> GetAllAsync()
        {
            return (await _uow.Sentences.GetAllAsync()).Select(AutoMapper.Mapper.Map<SentenceEntity, Sentence>);
        }

        public async Task<Sentence> FindAsync(int sentenceId)
        {
            return AutoMapper.Mapper.Map<Sentence>(await _uow.Sentences.GetAsync(sentenceId));
        }

        public async Task<Sentence> AddAsync(Sentence sentence)
        {
            await _uow.Sentences.AddAsync(AutoMapper.Mapper.Map<SentenceEntity>(sentence));
            var id = _uow.SaveChanges();
            sentence.SentenceId = id;
            return sentence;
        }

    }
}
