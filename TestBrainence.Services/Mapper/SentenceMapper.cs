﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using TestBrainence.Domain.Entities;
using TestBrainence.Models.Models;

namespace TestBrainence.Services.Mapper
{
    public class SentenceMapper : Profile
    {
        public SentenceMapper()
        {
            CreateMap<SentenceEntity, Sentence>().ReverseMap();
        }
    }
}
