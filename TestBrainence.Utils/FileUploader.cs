﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace TestBrainence.Utils
{
    public class FileUploader
    {
        private readonly IHostingEnvironment _env;

        public FileUploader(IHostingEnvironment env)
        {
            _env = env;
        }

        public string SaveFile(IFormFile file)
        {
            string filePath = Path.Combine(_env.WebRootPath, "sentences", file.FileName);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            using (var stream =  File.Create(filePath))
            {
                file.CopyTo(stream);
            }

            return filePath;
        }
    }
}
