﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using TestBrainence.Models.Models;

namespace TestBrainence.Utils
{
    public class TextParser
    {
        private readonly string _filePath;
        private readonly string _filter;

        public TextParser(string filePath,string filter)
        {
            _filePath = filePath;
            _filter = filter;
        }

        public List<Sentence> Parse()
        {
            List<Sentence> listseSentences = new List<Sentence>();
            string TextFile,sentence; int Index = 0, flag = 0, countWord;
            
                TextFile = File.ReadAllText(_filePath, Encoding.UTF8);

            foreach (var a in TextFile)
            {
                if (a == '.')
                {   //вибірка речення
                    sentence = TextFile.Substring(flag, Index - flag);

                    //перерахування кількості слів у речені без урахування регістра, та з урахуванням цілісності слова, а не його частини
                    countWord = Regex.Matches(sentence, @"\b"+_filter+@"\b", RegexOptions.IgnoreCase).Count;

                    //якщо в реченні знаходиться слово по фільтру, воно записується в список у зворотньому напрямку
                    if (countWord!=0)
                    listseSentences.Add(new Sentence(){Text = ReverseString(sentence),WordKey = _filter,Count = countWord});
                    flag = Index + 1;
                }
                Index++;
            }

            return listseSentences;
        }

        public string ReverseString(string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }

    }
}
