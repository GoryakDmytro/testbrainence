﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TestBrainence.Domain.Entities
{
    //structure Sentence table in DB

    public class SentenceEntity
    {
        [Key]
        public int SentenceId { set; get; }
        public string Text { set; get; }
        public string WordKey { set; get; }
        public int Count { set; get; }
    }
}
