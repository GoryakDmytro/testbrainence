﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestBrainence.Models.Models;

namespace TestBrainence.ViewModels
{
    public class SentenceViewModel
    {
        public List<Sentence> Sentences { get; set; }
    }
}
