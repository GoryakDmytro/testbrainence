﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp;
using TestBrainence.Models.Models;
using TestBrainence.Persistence;
using TestBrainence.Services;
using TestBrainence.Services.Contracts;
using TestBrainence.Utils;
using TestBrainence.ViewModels;


namespace TestBrainence.Controllers
{
    public class UploadPageController : Controller
    {
        
        private readonly ISentenceService _sentenceService;
        private readonly FileUploader _fileUploader;
        private TestBrainenceDbContext _context;

        public UploadPageController(ISentenceService sentenceService, FileUploader fileUploader, TestBrainenceDbContext context)
        {
            _sentenceService = sentenceService;
            _fileUploader = fileUploader;
            _context = context;
            
        }

        public async Task<IActionResult> Index()    
        {
            var sentences = (await _sentenceService.GetAllAsync()).ToList();
            var result = new SentenceViewModel
            {
                Sentences = sentences.OrderBy(c => c.SentenceId).ToList()
            };

            return View(result);
        }


        [HttpPost]
        public async Task<IActionResult> Index(IFormFile file, string filter)
        {
            string filePath =  _fileUploader.SaveFile(file);
            TextParser txtParser = new TextParser(filePath, filter);
            var sentences =  txtParser.Parse();

            foreach (var s in sentences)
            {
                var result = await _sentenceService.AddAsync(s);
            }

            return RedirectToAction("Index");
        }
    }
}
