﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using TestBrainence.Domain.Entities;

namespace TestBrainence.Persistence
{
    public class TestBrainenceDbContext : DbContext
    {
        public DbSet<SentenceEntity> Sentences { get; set; }

        public TestBrainenceDbContext(DbContextOptions<TestBrainenceDbContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
