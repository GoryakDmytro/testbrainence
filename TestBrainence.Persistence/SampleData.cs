﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using TestBrainence.Domain.Entities;

namespace TestBrainence.Persistence
{
    public class SampleData
    {
        public static void Initialize(TestBrainenceDbContext context)
        {
            if (!context.Sentences.Any())
            {
                context.Sentences.AddRange(
                    new SentenceEntity() { Text = "вінофелет имаремон ясилянімбо инов ,оншіпсу олшйорп овтсмойанз ешреП", WordKey = "успішно", Count = 1},
                    new SentenceEntity() { Text = "яіцяліпмок ашреп ясьтєушреваз оншіпсу иджваз еН", WordKey = "успішно", Count = 1 },
                    new SentenceEntity() { Text = "икепзеб їонйіцаідар яннещивдіп з ткеорп йинчіголоке ясвишреваз оншіпсу яннежучдів іноз у СЄ икмиртдіп аЗ", WordKey = "успішно", Count = 1 }
                   );
                context.SaveChanges();
            }
        }
    }
}
