﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestBrainence.Models.Models
{
    public class Sentence
    {
        public int SentenceId { set; get; }
        public string Text { set; get; }
        public string WordKey { set; get; }
        public int Count { set; get; }
    }
}
